package com.aladdin.swagger.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Aladdin
 * @date 2022/8/11 0:32
 */
@ApiModel("用户实体类")
public class User {

    @ApiModelProperty(value = "用户名", example = "123123", required = true)
    private String username;

    @ApiModelProperty(value = "密码", example = "aaa123", required = true)
    private String password;

    public String getUsername() {
        return username;
    }

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
