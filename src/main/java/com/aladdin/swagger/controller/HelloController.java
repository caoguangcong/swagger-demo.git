package com.aladdin.swagger.controller;

import com.aladdin.swagger.pojo.User;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Aladdin
 * @date 2022/8/9 22:51
 */
@Api("Hello 控制类")
@RestController
public class HelloController {

    @ApiOperation("测试query")
    @ApiResponses({@ApiResponse(code = 1000, message = "操作成功"),
            @ApiResponse(code = 9999, message = "系统异常"),
            @ApiResponse(code = 8888, message = "权限不足")})
    @ApiImplicitParams({@ApiImplicitParam(paramType = "query", dataType = "String", name = "str", value = "字符串", required = true)})
    @GetMapping("/hello")
    public String hello(String str) {
        return "Hello: " + str;
    }

    @ApiOperation("测试path")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "path", dataType = "Long", name = "id", value = "信息id", required = true)})
    @GetMapping("/hello2/{id}")
    public String hello2(@PathVariable("id") Long id) {
        return "Hello: " + id;
    }

    @ApiOperation("测试header")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "header", dataType = "Long", name = "id", value = "信息id", required = true)})
    @GetMapping("/hello3")
    public String hello3(@RequestHeader("id") Long id) {
        return "Hello: " + id;
    }

    @ApiOperation("测试form")
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "form", dataType = "Long", name = "id", value = "信息id", required = true) })
    @PostMapping(value = "/hello4", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String hello4(@RequestParam("id") Long id) {
        return "Hello: " + id;
    }

    @ApiOperation("测试body")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "body", dataType = "User", name = "user", value = "用户信息参数", required = true)})
    @PostMapping("/user")
    public User user(@RequestBody User user) {
        return user;
    }

}
