package com.aladdin.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

/**
 * @author Aladdin
 * @date 2022/8/9 22:57
 */
@Configuration
public class SwaggerConfig {

    /**
     * 配置 Swagger Docket 的 bean实例
     *
     * @return Docket 的 bean实例
     */
    @Bean
    public Docket docket(Environment environment) {

        //配置要显示 Swagger 的环境
        Profiles profiles = Profiles.of("dev", "test");
        //通过environment.acceptsProfiles()方法判断当前是否处在自己设定的环境中
        boolean flag = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //是否启动 Swagger，如果为 false，则 UI界面提示：【😱 Could not render e, see the console.】，无法在浏览器中访问 Swagger
                .enable(flag)
                //设置组名称，一个 Docket bean 对应一个组名称
                .groupName("阿拉丁")
                .select()
                /*
                RequestHandlerSelectors 配置扫描接口的方式
                    basePackage():指定要扫描的包（通常指定包扫描）
                    any():扫描全部
                    none():不扫描
                    withMethodAnnotation():扫描方法上的注解
                    withClassAnnotation():方法类上的注解
                 */
                .apis(RequestHandlerSelectors.basePackage("com.aladdin.swagger.controller"))
                /*
                PathSelectors 请求路径匹配的方式
                    regex():正则匹配
                    any():扫描全部
                    none():不扫描
                    ant():路径匹配
                 */
                //.paths(PathSelectors.ant("/aladdin/**"))
                .build();
    }

    /**
     * 配置 swagger 的信息：ApiInfo
     *
     * @return ApiInfo 的 bean实例
     */
    public ApiInfo apiInfo() {

        //作者信息
        Contact contact = new Contact("小聪", "https://blog.csdn.net/weixin_52610802?type=blog", "coder_cong@163.com");

        return new ApiInfo("Aladdin的SwaggerAPI文档",
                "不负韶华，未来可期",
                "V1.0",
                "https://blog.csdn.net/weixin_52610802?type=blog",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }

    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("分组1");
    }

    @Bean
    public Docket docket2() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("分组2");
    }

    @Bean
    public Docket docket3() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("分组3");
    }

}
